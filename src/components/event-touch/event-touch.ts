import { Component } from '@angular/core';

/**
 * Generated class for the EventTouchComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'event-touch',
  templateUrl: 'event-touch.html'
})
export class EventTouchComponent {

  event: string;

   // variable contenant Canvas principal
   canvas;
  // variable contenant le context du canvas principal

  mouseEvent = ['mousedown', 'mousemove' , 'mouseup']
  touchEvent = ['touchstart', 'touchmove' , 'touchend']
  tabEvent;

  typeEvent = false;
  context;

  constructor() {
    
    this.event = 'No Event';
    this.catchEvent();
  }

  catchEvent(){
  


    //#region  Initialisation du Canvas avec image
    setTimeout(() => {
     if(this.typeEvent)
 {
   this.tabEvent = this.mouseEvent
 }
 else
 {
   this.tabEvent = this.touchEvent
 }
     //this if statment checks if the id "thisCanvas" is linked to something
     if (document.getElementById("MyCanvas") != null) {
       //do what you want

       this.canvas = document.getElementById("MyCanvas") as HTMLCanvasElement;
       if (this.canvas)
         this.context = this.canvas.getContext("2d");

        //  touchStart
         this.canvas.addEventListener(this.tabEvent[0] , evt => {
           console.log(this.tabEvent[0])
           this.event = this.tabEvent[0];
           evt.preventDefault();
           var touches = evt.changedTouches;
            console.table('___________ touch Start ___________', touches);

         });

        //  touchMove
         this.canvas.addEventListener(this.tabEvent[1], evt => {
           console.log(this.tabEvent[1])
           this.event = this.tabEvent[1];
           evt.preventDefault();
           var touches = evt.changedTouches;
            console.table('___________ touch Move ___________', touches);
         });

        //  touchEnd
         this.canvas.addEventListener(this.tabEvent[2], evt => {
           console.log(this.tabEvent[2])
           this.event = this.tabEvent[2];
           evt.preventDefault();
           var touches = evt.changedTouches;
            console.table('___________ touch End ___________', touches);
         });


     }
   }, 1000);
   //#endregion
}


}
