import { NgModule } from '@angular/core';
import { EventTouchComponent } from './event-touch/event-touch';
@NgModule({
	declarations: [EventTouchComponent],
	imports: [],
	exports: [EventTouchComponent]
})
export class ComponentsModule {}
